// Validate all required fields before submission.
// All fields are tested for validity when they lose focus, so
// don't worry about non-required fields.
//
// If any of the fields possess the "error" class, we know
// there's something wrong. Abort the submission.

var DriggValidateBeforeSubmit = function () {
	DriggValidateBodyFull( true );
	DriggValidateTitleFull( true );
	DriggValidateURL( $("#edit-url"), "Story's URL", 'sURL', true);
	if ( $(".error").length > 0 ) {
		return false;
	}
	return true;
}

// cname: The error "class name"
// error: The error message to display
//
// Generate an error message in a standard error message box.
// The error has an error name (cname) and an error message.
// The cname is assigned as the class name to the error message.
// In that way, everything that can generate an error has a distinct
// error message location. That message is generated, cleared, or
// modified based on the cname.

function DriggAddError( cname, error ) {
	var msgs = $(".messages.error ul");

	// If the error display area doesn't exist, create it.
	// Arbitrarily, it is created after the div with class "help"
	if ( msgs.length == 0 ) {
		$(".messages.error").remove();
		$("div.help").after('<div class="messages error"><ul></ul></div>' );
		msgs = $(".messages.error ul");
	}
	// Remove any current messages with a class of <cname>
	msgs.find("."+cname).remove();

	// Create a new error item identified by <cname>
	msgs.append('<li class="' + cname + '">' + error + '</li>');
}


// cname: The "error class" identifier
//
// Remove the error message identified by <cname>.
// If there are no more error messages, remove the error
// message box.

function DriggRemoveError( cname ) {
	var msgs = $(".messages.error ul");

	msgs.find("." + cname).remove();
	if( msgs.children().length == 0 ) {
		$(".messages.error").remove();
	}
}

// Validate the "Story description" text area. Do this by
// calling DriggValidateBodyFull. The "false" indicates 
// this is not a full check-- don't generate an error if
// the field is empty.

var DriggValidateBody = function() {
	DriggValidateBodyFull( false );
}

// check: Full check flag
//
// Validate the "Story description" text area. If <check> is true,
// generate an error if the field is empty. Otherwise, only generate
// an error if the field contains characters, but does not contain
// the minimum required number of characters.
//
// Returns "true" if the field validates, "false" otherwise.

var DriggValidateBodyFull = function( check ) {
	var body = $("#edit-body");

	if ( check || body.val().length > 0 )
	{
		check = true;
	}
	else {
		DriggRemoveError("bodyErr");
		body.removeClass("error");
	}

	if ( check ) {
		if ( $("#edit-body").val().length < drigg_body_minimum_min_length ) {
			DriggAddError("bodyErr", "The description must be at least " + 
				drigg_body_minimum_min_length + " characters long.");
			$("#edit-body").addClass("error");
			return false;
		}
		else {
			DriggRemoveError("bodyErr");
			$("#edit-body").removeClass("error");
			return true;
		}
	}
	else {
		return false;
	}
}

// Validate the "Story's title" text input. Do this by
// calling DriggValidateTitleFull. The "false" indicates 
// this is not a full check-- don't generate an error if
// the field is empty.

var DriggValidateTitle = function( ) {
	DriggValidateTitleFull( false );
}

// check: Full check flag
//
// Validate the "Story's title" text input. If <check> is true,
// generate an error if the field is empty. Otherwise, only generate
// an error if the field contains characters, but does not contain
// the minimum required number of characters.
//
// Returns "true" if the field validates, "false" otherwise.

var DriggValidateTitleFull = function( check ) {
	var title = $("#edit-title");
	if ( check || title.val().length > 0 )
	{
		check = true;
	}
	else {
		DriggRemoveError("titleErr");
		title.removeClass("error");
	}
	if ( check )
	{
		if ( $("#edit-title").val().length < drigg_title_minimum_length ) {
			DriggAddError("titleErr", "The title must be at least "
					+ drigg_title_minimum_length + " characters.");
			$("#edit-title").addClass("error");
			return false;
		}
		else {
			DriggRemoveError("titleErr");
			$("#edit-title").removeClass("error");
			return true;
		}
	}
	else {
		return false;
	}
}

// Check for related links.
//
// Build an AJAX request from the title and body (description).
// This should return some HTML containing related links.
//
// Only use the title and body if they are valid, and contain
// data.

var DriggRelatedLinks = function () {
        var entry = $(this);
        var args = {};
	var title = $("#edit-title");

	if ( !DriggValidateBodyFull(false) ) {
		$("#related_links_box").html('');
		return;
	}
	if ( DriggValidateTitleFull(false) ) {
		args.text = title.val() + " ";
	}
	else {
		args.text = '';
	}

        args.operation = 'related_links';
        args.text += entry.val();
        $.ajax({
                data     : args,
                url      : karma_base_path+"drigg/handle",
                type     : 'get',
                datatype : 'html',
                success  : function (result) {
			$("#related_links_box").html(result);
                },
                error: function( xhr, msg ) {
                },
                complete: function( xhr, stat ) {
                }
        });

}

// Validate the "Trackback URL" text input. Do this by
// calling DriggValidateURL. (SEE DriggValidateURL for
// a complete description of the arguments.) 

var DriggValidatePingURL = function() {
	DriggValidateURL( this, 'Trackback URL', 'tbURL', false);
}

// Validate the "Story's URL" text input. Do this by
// calling DriggValidateURL. (SEE DriggValidateURL for
// a complete description of the arguments.) 

var DriggValidateStoryURL = function() {
	DriggValidateURL( this, "Story's URL", 'sURL', false);
}

// elem: The text input to validate
// prefix: A string to display in the error box
// errtype: A unique identifier (<cname> in DriggCreateError
//          and DriggRemoveError) 
// check: Full check flag
//
// Validates the text contained in <elem> to make sure the URL
// is valid. This is done via the server, so we pass it in
// with AJAX.
//
// If the entry is not a valid URL, generate an error of class
// <errtype>. The error message begins with "prefix," which
// should help distinguish the error for the user.
//
// If <check> is false, don't bother reporting if the field is
// empty. This helps make things a little nicer when navigating
// the form.

var DriggValidateURL = function(elem, prefix, errtype, check) {
	var entry = $(elem);
	var args = {};
	args.operation = 'validate_url';
	args.url = entry.val();

	if ( args.url == '' )
	{
		if ( !/required/.test(entry[0].className) ) {
			check = false;
		}
		DriggRemoveError(errtype);
		entry.removeClass("error");
	}
	else {
		check = true;
	}

	if ( check ) {
		if ( args.url == '' ) {
			DriggAddError(errtype, prefix + ' is required');
			entry.addClass("error");
	
			return false;
		}

		$.ajax({
			data     : args,
                	url      : karma_base_path+"drigg/handle",
			type     : 'get',
			datatype : 'html',
			success  : function (result) {
				if ( result == 'success' ) {
					entry.removeClass("error");
					DriggRemoveError(errtype);
				}
				else {
					DriggAddError(errtype, prefix + ': ' + result);
					entry.addClass("error");
				}
			},
			error: function( xhr, msg ) {
				entry.addClass("error");
			},
			complete: function( xhr, stat ) {
			}	
		});
	
	}
}

// This is executed on document load.
//
// If this appears to be a drigg node form, wire up the text
// entries for validation.

$(function(){
	if ( $("#edit-drigg-node-form").length > 0 )   {
		$("#edit-trackback-ping-url").blur(DriggValidatePingURL);
		$("#edit-body").blur(DriggRelatedLinks);
                if(! drigg_editing_node){
			$("#edit-url").blur(DriggValidateStoryURL);
                }
		$("#edit-title").blur(DriggValidateTitle);
		$("#node-form").submit(DriggValidateBeforeSubmit);
	}
});


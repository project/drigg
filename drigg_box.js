/*

To call this block:

<script type="text/javascript">
	var box_fg_color = "0x0000";
	var box_bg_color = "0x0CCC";
	var text_fg_color = "4";
	var text_bg_color = "5";
	var box_width = "500px";
	var box_height = "600px";
        var section = "All";
</script>
<script type="text/javascript" src="http://www.drigg.org/sites/www.drigg.org/modules/drigg/drigg_box.js"></script>
*/

/* 
   ---------------------------------------
   --          DEFAULT VALUES           --
   --       (Change here as needed)     --
   ---------------------------------------
*/
var def_fetch_host;
var def_fetch_path="/drigg/handle?operation=top";


var def_section="All";
var def_how_many=10;
var def_show_karma=1;
var def_bg_color ="FFFFFF";
var def_text_color="333333";
var def_score_bg_color="FFFFCC";
var def_score_text_color="000000";
var def_link_color="333333";
var def_border_style="classic";
var def_border_color="000000";
var def_score_border_color="000000";
var def_text_size="8";

/* 
   -------------------------------------------
   --       END OF DEFAULT VALUES           --
   -------------------------------------------
*/
var iframeTmp = document.createElement("iframe");
var iframeObj = document.body.appendChild(iframeTmp);

if ( !def_fetch_host ) {
	def_fetch_host = iframeObj.previousSibling.src.replace( /(http:\/\/)/, '' );
	def_fetch_host = def_fetch_host.replace( /\/.*/, '' );
}
var def_fetch_url="http://"+def_fetch_host+def_fetch_path;



var db_list = {
	"section"      : 
          (typeof(section)!='undefined')?section:def_section,


	"bg_color" : 
          (typeof(bg_color)!='undefined')?bg_color:def_bg_color,
	"text_color" : 
          (typeof(text_color)!='undefined')?text_color:def_text_color,
	"score_bg_color": 
          (typeof(score_bg_color)!='undefined')?score_bg_color:def_score_bg_color,
	"score_text_color": 
          (typeof(score_text_color)!='undefined')?score_text_color:def_score_text_color,
	"link_color": 
          (typeof(link_color)!='undefined')?link_color:def_link_color,

        "border_style":
          (typeof(border_style)!='undefined')?border_style:def_border_style,
        "border_color":
          (typeof(border_color)!='undefined')?border_color:def_border_color,
        "score_border_color":
          (typeof(score_border_color)!='undefined')?score_border_color:def_score_border_color,
        "text_size":
          (typeof(text_size)!='undefined')?text_size:def_text_size,
        "how_many":
          (typeof(how_many)!='undefined')?how_many:def_how_many,
        "show_karma":
          (typeof(show_karma)!='undefined')?show_karma:def_show_karma,

}
iframeTmp.src  = def_fetch_url;
for (var db_args in db_list) {
	iframeTmp.src += "&"+db_args+"="+db_list[db_args];
}
iframeTmp.frameBorder = "0";
iframeTmp.height = (typeof(box_height)!='undefined')?box_height:"300px";
iframeTmp.width = (typeof(box_width)!='undefined')?box_width:"400px";
iframeTmp.name = "drigg_box";

delete def_fetch_host;
delete def_fetch_path;


function custom_url_rewrite($op, $result, $path, $path_language=NULL) {

  $term_list=variable_get("drigg_section_list",array());
  $tag_vid=variable_get('drigg_tag_vid','');

  // This is OUTGOING stuff
  if($op == 'alias'){


    // Make outgoing links look right
    if (preg_match('|^node/([^/]+)(.*)$|', $path, $matches)) {
      $n = $matches[1];
      $rest = $matches[2];

      if(is_numeric($n)){
        $res= drigg_settings_lookup_drigg_node_by_dnid($n);
        if($res){
          // Return the right URL
          $safe_section=$res->safe_section;
          $title_url=$res->title_url;
          // I hate this, but we need to have it because you never know
          $safe_section = $safe_section != '' ? $safe_section : 'All';
          return("$safe_section/$title_url$rest");
        }
      }
    }

    // Make TAG links look right
    if (preg_match('|^taxonomy/term/([0-9]+)(/?)(.*)$|', $path, $matches)) {

      if($matches[3] == ''){
        $tid = $matches[1];
        $res=drigg_settings_lookup_term_name_by_tid($tid);
        if($res){
          $res=urlencode($res);
          return("tag/".$res);
        }
      }

    }


    // Make users look right 
    if (preg_match('|^user/([^/]+)(.*)$|', $path, $matches)) {
      $uid = $matches[1];
      $rest = $matches[2];
      $res=drigg_settings_lookup_user_name_by_uid($uid);
      if($res){
        $res=urlencode($res);
        return("users/${res}${rest}");
      }
    }
       

  }

  // This is INCOMING requests
  if($op == 'source'){

    // Change the INCOMING links, so that people can view
    // www.a.com/Community/Embedded_Linux_pioneer_offers_webinar/ or even
    // www.a.com/Community/Embedded_Linux_pioneer_offers_webinar/edit
    // 
    if(preg_match('%^(.*?)/([^/]+)(.*)$%', $path, $matches)){
      $cat = $matches[1];
      $title_url = $matches[2];
      $rest = $matches[3];
      if( isset($term_list['by_safe_name'][$cat]) || $cat== 'All'){
        $res=drigg_settings_lookup_drigg_node_by_title_url($matches[2]);
        if($res){
          $nid=$res->dnid;
          $safe_section = $res->safe_section;

          // Check that the article's category matches the requested
          // one. This is not strictly necessary
          /* if($cat != "All" && $cat != $safe_section){
            return 'file_not_found';
          }*/ 

          return 'node/'.$nid.$rest;
        }
      }
    }


    // Change /tag/pippo into taxonomy/term/33
    if(preg_match('%^tag/([^/]+)$%', $path, $matches)){
      $tag = urldecode($matches[1]);
      $tid= drigg_settings_lookup_term_tid_by_name($tag);
      
      if($tid){
        return 'taxonomy/term/'.$tid;
      }
    }

    // Change /users/pippo into user/33
    if (preg_match('|^users/([^/]+)(.*)$|', $path, $matches)) {
      $name = urldecode($matches[1]);
      $rest = $matches[2];
      $res=drigg_settings_lookup_user_uid_by_name($name);

      if($res){
        return("user/${res}${rest}");
      }
    }

  }

  return $result;
}


function drigg_settings_lookup_term_tid_by_name($name){
  static $cache;

  if(!$cache[$name]){
    $res=db_result(db_query("SELECT tid FROM {term_data} WHERE name ='%s'",$name));
    $cache[$name]=$res;
  }
  return $cache[$name];
}

function drigg_settings_lookup_term_name_by_tid($tid){
  static $cache;


  if(!$cache[$tid]){
    $res=db_result(db_query("SELECT name FROM {term_data} WHERE tid =%d",$tid));
    $cache[$tid]=$res;
    #drupal_set_message("HERE: SELECT name FROM {term_data} WHERE tid =$tid: $res");

  }
  return $cache[$tid];
}

function drigg_settings_lookup_drigg_node_by_dnid($dnid){
  static $cache;

  if(!$cache[$dnid]){
    $res=db_fetch_object(db_query("SELECT safe_section,title_url FROM {drigg_node} WHERE dnid ='%d'",$dnid));
    $cache[$dnid]=$res;
  }
  return $cache[$dnid];
}

function drigg_settings_lookup_drigg_node_by_title_url($title_url){
  static $cache;

  if(!$cache[$title_url]){
        $res = db_fetch_object(db_query("SELECT dnid,safe_section FROM {drigg_node} WHERE title_url ='%s'",$title_url));
    $cache[$title_url]=$res;
  }
  return $cache[$title_url];
}


function drigg_settings_lookup_user_uid_by_name($name){
  static $users;

  if(!$users[$name]){
    $uid= db_result(db_query("SELECT uid FROM {users} WHERE name ='%s'",$name));
    $users[$name]=$uid;
  }
  return $users[$name];
}
function drigg_settings_lookup_user_name_by_uid($uid){
  static $users;

  if(!$users[$uid]){
    $name= db_result(db_query("SELECT name FROM {users} WHERE uid =%d",$uid));
    $users[$uid]=$name;
  }
  return $users[$uid];
}


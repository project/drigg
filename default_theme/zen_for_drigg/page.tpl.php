<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

<?php if(module_exists('simple_karma')){ ?>
<?print simple_karma_insert_js_css(); ?>
<?php }  ?>

</head>

<?php /* different classes allow for separate theming of the home page */ ?>
<body class="<?php print $body_classes; ?>">

  <div id="page">
    <div id="header">

      <div id="skip-nav"><a href="#content"><?php print t('Skip to Main Content'); ?></a></div>

      <div id="logo-title">

        <?php print $search_box; ?>
        <?php if (!empty($logo)): ?>
          <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
          </a>
        <?php endif; ?>

        <div id="name-and-slogan">

        <?php if (!empty($site_name)): ?>
          <div id='site-name'><strong>
            <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>" rel="home">
              <?php print $site_name; ?>
            </a>
          </strong></div>
        <?php endif; ?>

        <?php if (!empty($site_slogan)): ?>
          <div id='site-slogan'>
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>

        </div> <!-- /name-and-slogan -->

      </div> <!-- /logo-title -->

      <div id="navigation" class="menu<?php if ($primary_links) { print " withprimary"; } if ($secondary_links) { print " withsecondary"; } ?> ">
        <?php if (!empty($primary_links) && FALSE ): ?>
          <div id="primary" class="clear-block">
            <?php print theme('links', $primary_links); ?>
          </div>
        <?php endif; ?>

          <?php if(module_exists('drigg')){ ?>
            <div id="primary" class="clear-block">
            <?php print theme_drigg_sections() ?>
            </div>
          <?php } ?>



        <?php if (!empty($secondary_links)): ?>
          <div id="secondary" class="clear-block">
            <?php print theme('links', $secondary_links); ?>
          </div>
        <?php endif; ?>
      </div> <!-- /navigation -->

      <?php if (!empty($header) || !empty($breadcrumb)): ?>
        <div id="header-region">
          <?php print $breadcrumb; ?>
          <?php print $header; ?>
        </div> <!-- /header-region -->
      <?php endif; ?>

    </div> <!-- /header -->

    <div id="container" class="clear-block">



     <?php if( $show_ads && ! ( arg(0) == 'node' && is_numeric(arg(1)) ) ) { ?>
       <div class="googleads_top">
         <script type="text/javascript"><!--
         google_ad_client = "pub-4408039167229399";
         google_ad_width = 468;
         google_ad_height = 60;
         google_ad_format = "468x60_as";
         google_ad_type = "text_image";
         google_color_border = "FFFFFF";
         google_color_bg = "FFFFFF";
         google_color_link = "2763A5";
         google_color_url = "555555";
         google_color_text = "000000";
         //-->
         </script>
         <script type="text/javascript"
           src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
         </script>
       </div>
     <?php } ?>





      <?php if (!empty($sidebar_left)): ?>
        <div id="sidebar-left" class="column sidebar">
          <?php print $sidebar_left; ?>
        </div> <!-- /sidebar-left -->
      <?php endif; ?>

      <div id="main" class="column"><div id="squeeze" class="clear-block">
        <?php if (!empty($mission)): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>
        <?php if (!empty($content_top)):?>
          <div id="content-top"><?php print $content_top; ?></div>
        <?php endif; ?>
        <div id="content">
          <?php if (!empty($title) && !( arg(0) == 'node' && is_numeric(arg(1)))) : ?>
            <h1 class="title"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php if (!empty($tabs)): ?>
            <div class="tabs"><?php print $tabs; ?></div>
          <?php endif; ?>
          <?php print $help; ?>
          <?php print $messages; ?>
          <?php print $content; ?>
          <?php if (!empty($feed_icons)): ?>
            <div class="feed-icons"><?php print $feed_icons; ?></div>
          <?php endif; ?>
        </div> <!-- /content -->
        <?php if (!empty($content_bottom)): ?>
          <div id="content-bottom"><?php print $content_bottom; ?></div>
        <?php endif; ?>
      </div></div> <!-- /squeeze /main -->

      <?php if (!empty($sidebar_right)): ?>
        <div id="sidebar-right" class="column sidebar">

          <?php if(module_exists('drigg')){ ?>
          <?php print drigg_get_submit_button(); ?>
          <?php } ?>

          <?php print $sidebar_right; ?>

             <?php if( $show_ads) { ?>
             <div class="googleads_left">
               <script type="text/javascript"><!--
               google_ad_client = "pub-4408039167229399";
               google_ad_width = 160;
               google_ad_height = 600;
               google_ad_format = "160x600_as";
               google_ad_type = "text_image";
               google_color_border = "ffffff";
               google_color_bg = "ffffff";
               google_color_link = "2763A5";
               google_color_url = "008000";
               google_color_text = "000000";
               //--></script><script type="text/javascript"
                 src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
               </script>
             </div>
             <?php } ?>



        </div> <!-- /sidebar-right -->
      <?php endif; ?>

    </div> <!-- /container -->

    <div id="footer-wrapper">
      <div id="footer">
        <?php print $footer_message; ?>
      </div> <!-- /footer -->
    </div> <!-- /footer-wrapper -->

    <?php print $closure; ?>

  </div> <!-- /page -->

</body>
</html>

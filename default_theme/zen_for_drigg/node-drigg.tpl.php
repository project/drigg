<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
  <?php print simple_karma_show_form($node,'n',3); ?>

  <div class="offset">
    <?php if ($page == 0 || TRUE): ?>

      <h2 class="title">
        <?php if($node->teaser){ ?>
        <a href="<?php print $node_url ?>"><?php print $title; ?></a>
        <?php } else { ?>
        <a href="<?php print $node->url ?>"><?php print $title; ?></a>
        <?php } ?>
      </h2>
    <?php endif; ?>

    <?php if ($picture) print $picture; ?>

    <?
      $section_id=drigg_section_id_by_safe_name($node->safe_section);
      $section_name=drigg_section_name_by_safe_name($node->safe_section);
      $section_link=l($section_name, 'taxonomy/term/'.$section_id);
    ?>


  <?php if ($submitted): ?>
    <div class="submitted">
      <?php print $submitted ?>
    </div>
  <?php endif; ?>

  <?php if (count($taxonomy)): ?>
    <div class="taxonomy"><?php print t(' in ') . $terms ?></div>
  <?php endif; ?>

  <div class="content">
    <?php print $content; ?>
  </div>

   <?php print drigg_embedded_contents($node,$node_url,$teaser);?>

      <div class="storydata">
        <?php print theme('user_picture',user_load(array('uid'=>$node->uid)));?>
        Submitted by <?php print theme('username', $node); ?><br />
        in <?php print $terms; ?><br />

        <span class="dates">
          <span class="creation_date"><?php print drigg_get_created_string($node); ?></span>
          -
          <span class="promotion_date"><?php print drigg_get_promote_string($node); ?></span>
        </span>
      </div>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

</div>

</div>






<?php if( $show_ads && ! $teaser) { ?>
  <div class="googleads_node">
    <script type="text/javascript"><!--
    google_ad_client = "pub-4408039167229399";
    google_alternate_ad_url = "http://www.freesoftwaremagazine.com/images/banner_space_for_rent.gif";
    google_ad_width = 300;
    google_ad_height = 250;
    google_ad_format = "300x250_as";
    google_ad_type = "text_image";
    //2007-05-17: blogfloatingsquare
    google_ad_channel = "6211916278";
    google_color_border = "FFFFFF";
    google_color_bg = "FFFFFF";
    google_color_link = "2763A5";
    google_color_url = "555555";
    google_color_text = "000000";
    //--></script>
    <script type="text/javascript"
      src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
  </div>
<?php }  ?>



<?php if(!$node->teaser){ ?>
  <?php print drigg_get_article_menu(); ?>
<?php }  ?>


